from django.contrib import admin
from .models import Pessoa

class ListarPessoas(admin.ModelAdmin):
    list_display = ('id', 'nome', 'senha')
    list_display_link = ('id', 'nome')


admin.site.register(Pessoa, ListarPessoas)
