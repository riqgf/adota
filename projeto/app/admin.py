from django.contrib import admin
from .models import Gato
# Register your models here.


class ListarGatos(admin.ModelAdmin):
    list_display = ('id', 'nome')
    list_display_links = ('id', 'nome')
    search_fields = ('nome',)
    list_per_page = 5

admin.site.register(Gato, ListarGatos)
