from django.shortcuts import render
from django.views.generic import ListView
from app.models import Gato


# Create your views here.



class Index(ListView):
    num_gatos = Gato.objects.all().count()
    model = Gato
    template_name = 'index.html'
    context_object_name = 'gatos'

    paginate_by = 2


def cadastro(request):
    num_gatos = Gato.objects.all().count()
    context = {
        'num_gatos': num_gatos
    }

    return render(request, 'cadastro.html')
